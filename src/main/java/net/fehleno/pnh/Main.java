package net.fehleno.pnh;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Main
{
	private static final int DELAY = 600;
	private static final int DELAY_SILENCE = 100;
	private static Map<String, Double> notes = new HashMap<>();
	
	public static void main(String[] args) throws IOException
	{
		List<String> noteLines = Files.readAllLines(Paths.get("./notes.txt"));
		
		for (String note : noteLines)
		{
			int index = note.indexOf("\t");
			String noteLetter = note.substring(0, index);
			double noteFrequency = Double.parseDouble(note.substring(index + 1, note.length()));
			notes.put(noteLetter, noteFrequency);
		}
		List<String> noteInput = new ArrayList<String>(List.of(args));
		noteInput.removeIf(letter -> letter.equals("–"));
		
		for (String note : noteInput)
		{
			double noteFrequency = notes.get(note);
			System.out.println("tone(buzzerPin, " + noteFrequency + "); //" + note);
			System.out.println("delay (" + DELAY + ");");
			System.out.println("noTone(buzzerPin);");
			System.out.println("delay (" + DELAY_SILENCE + ");");
			
		}
	}
	
}
